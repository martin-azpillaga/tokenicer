// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "forge-std/Test.sol";
import "../source/contracts/PooledToken.sol";
import "./Polygon.sol";

contract TestPooledToken is Test
{
	PooledToken token;
	Chain chain;

	function setUp () external
	{
		chain = new Polygon();
	}

	function testCreation () external
	{
		token = new PooledToken{ value: 0.1 ether }("Minim", "MNM", 10, 1, chain.exchange());
		emit log_address(address(token.token_a()));
		token.withdraw();
	}
}
