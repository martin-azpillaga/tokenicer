// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "./interfaces/Chain.sol";

contract Polygon is Chain
{
	Token public fiatBackedStablecoin = Token(0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174);
	Exchange public exchange = Exchange(0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506);
}
