showCode.onclick = () =>
{
	if (sourceCode.style.display === "none")
	{
		sourceCode.style.display = "block";
	}
	else
	{
		sourceCode.style.display = "none";
	}
}
iconUrl.style.display = "none";
updateIcon.style.display = "none";
iconImage.onclick = () =>
{
	iconImage.style.display = "none";
	iconUrl.style.display = "block";
	//updateIcon.style.display = "block";
}

updateIcon.onclick = async () =>
{
	iconImage.style.display = "initial";
	iconUrl.style.display = "none";
	updateIcon.style.display = "none";
}

iconUrl.onclick = () =>
{
	iconUrl.select();
}
iconUrl.onblur = async () =>
{
	const response = await fetch(iconUrl.value);
	if (response.ok)
	{
		console.log("OK");
		iconUrl.style.border = "1px solid green";
		iconImage.src = iconUrl.value;
		setTimeout(() =>
		{
			iconImage.style.display = "initial";
			iconUrl.style.display = "none";
			updateIcon.style.display = "none";
		}, 1000);
	}
	else
	{
		console.warn("Not ok");
		iconUrl.style.border = "1px solid red";
	}
}

symbol.onchange = () =>
{
	createdToken.textContent = symbol.value;
}

deploy.onclick = async () =>
{
	const provider = new ethers.providers.Web3Provider(ethereum);

	await ethereum.request({method:"eth_requestAccounts"});

	const signer = await provider.getSigner();

	const response = await fetch("PooledToken.json");
	const json = await response.json();

	const factory = new ethers.ContractFactory(json.abi, json.bytecode, signer);
	const deployment = await factory.deploy
	(
		identifier.value || identifier.placeholder,
		symbol.value || symbol.placeholder,
		decimals.value || decimals.placeholder,
		price.value || price.placeholder,
		exchange.value || exchange.placeholder,
		{ value : BigInt(value.value || value.placeholder) }
	);
	const contract_address = (await deployment.deployed()).address;

	const contract = new ethers.Contract(contract_address, json.abi, signer);

	const token_address = await contract.token_a();

	await ethereum.request({method: "wallet_watchAsset", params:{type: "ERC20", options:
	{
		address: token_address,
		symbol: symbol.value || symbol.placeholder,
		decimals: decimals.value || decimals.placeholder,
		image: icon.value || icon.placeholder
	}}});

	if (ethereum.chainId === "0x13881" && (exchange.value === exchange.placeholder || exchange.value === ""))
	{
		result.textContent = `You may swap native tokens for the created token @ https://app.sushi.com/swap?inputCurrency=ETH&outputCurrency=${token_address}&chainId=80001`;
	}

	const codeResponse = await fetch("flat.sol");
	const code = await codeResponse.text();


	const headers = {"Content-Type": "application/x-www-form-urlencoded"};
	const apikey = "Z3XQG7TRZ2DFET23YJBB7SQ31KYCN85A8D";

	setTimeout(async () =>
	{
		const ackResponse = await fetch("https://api-mumbai.polygonscan.com/api", {method: "POST", headers, body: new URLSearchParams
		({
			apikey,
			module: "contract",
			action: "verifysourcecode",
			sourceCode: code,
			contractaddress: contract_address,
			codeformat: "solidity-single-file",
			contractname: "PooledToken",
			compilerversion: "v0.8.16",
			optimizationUsed: 1,
			constructorArguments: "00000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e0000000000000000000000000000000000000000000000000000000000000001200000000000000000000000000000000000000000000000000000000000000010000000000000000000000001b02da8cb0d097eb8d57a175b88c7d8b479975060000000000000000000000000000000000000000000000000000000000000005546f6b656e0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003544b4e0000000000000000000000000000000000000000000000000000000000"
		})});

		const ack = await ackResponse.json();

		setTimeout(async () =>
		{
			const r = await fetch("https://api-mumbai.polygonscan.com/api", { method: "POST", headers, body: new URLSearchParams
			({
				apikey,
				module: "contract",
				action: "checkverifystatus",
				guid: ack.result
			})});

			const validation = await r.json();
			if (validation.message === "OK" || validation.result === "Already Verified")
			{
				result.textContent = "Contract verified";
			}
		}, 100000);
	},30000);
}

const chainId = parseInt(ethereum.chainId, 16);
const chainResponse = await fetch(`https://raw.githubusercontent.com/ethereum-lists/chains/master/_data/chains/eip155-${chainId}.json`)
const chain = await chainResponse.json();

console.log(chain);
deploy.textContent = `Deploy to ${chain.title || chain.name}`;
for (const element of document.querySelectorAll(".nativeCoin"))
{
	element.textContent = chain.nativeCurrency.name;
}
verify.textContent = `Verify in ${chain.explorers[0].name}`;
apikey.placeholder = `${chain.explorers[0].name} api key`;
