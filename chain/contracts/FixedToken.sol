// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "./interfaces/Token.sol";

contract FixedToken is Token
{
	address public owner;

	string public name;
	string public symbol;
	uint8 public decimals;
	uint public totalSupply;

	mapping(address => uint) public balanceOf;
	mapping(address => mapping(address => uint)) public allowance;

	event Transfer (address indexed from, address indexed to, uint amount);
	event Approval (address indexed from, address indexed to, uint amount);

	constructor (string memory _name, string memory _symbol, uint8 _decimals, uint _totalSupply)
	{
		owner = msg.sender;

		name = _name;
		symbol = _symbol;
		decimals = _decimals;
		totalSupply = _totalSupply;

		balanceOf[owner] = totalSupply;
	}

	function approve (address target, uint amount) public returns (bool)
	{
		allowance[msg.sender][target] = amount;

		emit Approval(msg.sender, target, amount);

		return true;
	}

	function transfer (address target, uint amount) public returns (bool)
	{
		doTransfer(msg.sender, target, amount);

		return true;
	}

	function transferFrom (address from, address to, uint amount) external
	{
		if (from != msg.sender)
		{
			allowance[from][msg.sender] -= amount;
		}

		doTransfer(from, to, amount);
	}

	function doTransfer (address from, address to, uint amount) internal
	{
		balanceOf[from] -= amount;
		balanceOf[to] += amount;

		emit Transfer(from, to, amount);
	}
}
