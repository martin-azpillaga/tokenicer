// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "./Exchange.sol";

interface Chain
{
	function exchange() external returns (Exchange);
	function fiatBackedStablecoin() external returns (Token);
}
