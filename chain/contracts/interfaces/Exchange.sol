// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "./Token.sol";
import "./Factory.sol";

interface Exchange
{
	function WETH() pure external returns (Token);
	function factory() pure external returns (Factory);

	function addLiquidity(address a, address b, uint a_desired, uint b_desired, uint a_min, uint b_min, address to, uint deadline) external returns (uint amountA, uint amountB, uint liquidity);
	function removeLiquidity(address a, address b, uint liquidity, uint a_min, uint b_min, address to, uint deadline) external returns (uint amountA, uint amountB);

	function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts);
	function swapTokensForExactTokens(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts);

	function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline) external payable returns (uint[] memory amounts);
	function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external payable returns (uint[] memory amounts);
}
