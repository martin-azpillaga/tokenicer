// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "./Token.sol";

interface Factory
{
	function getPair (address, address) external returns (address);
}
