// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "forge-std/Test.sol";
import "./interfaces/Token.sol";
import "./interfaces/Exchange.sol";
import "./FixedToken.sol";

contract PooledToken is Test
{
	address public owner;

	Exchange public exchange;
	Token public token_a;
	Token public token_b;
	Token public lp_token;

	constructor (string memory _name, string memory _symbol, uint8 _decimals, uint _price, Exchange _exchange) payable
	{
		owner = msg.sender;

		exchange = _exchange;

		token_b = exchange.WETH();

		NativeToken(address(token_b)).deposit{value: msg.value}();

		uint token_b_amount = msg.value;
		uint token_a_amount = msg.value / (_price * 10**(18-_decimals));

		token_a = new FixedToken(_name, _symbol, _decimals, token_a_amount);

		token_a.approve(address(exchange), token_a_amount);
		token_b.approve(address(exchange), token_b_amount);

		exchange.addLiquidity(address(token_a), address(token_b), token_a_amount, token_b_amount, 0, 0, address(this), block.timestamp);

		lp_token = Token(Factory(exchange.factory()).getPair(address(token_a), address(token_b)));
	}

	function withdraw () external
	{
		require(msg.sender == owner);

		uint lp_token_amount = lp_token.balanceOf(address(this));

		lp_token.approve(address(exchange), lp_token_amount);

		(uint amountA, uint amountB) = exchange.removeLiquidity(address(token_a), address(token_b), lp_token_amount, 0, 0, address(this), block.timestamp);

		token_a.transfer(owner, amountA);
		token_b.transfer(owner, amountB);
	}

	function path (Token from, Token to) pure internal returns (address[] memory)
	{
		address[] memory result = new address[](2);
		result[0] = address(from);
		result[1] = address(to);
		return result;
	}
}
