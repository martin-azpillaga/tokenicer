# Tokenicer

Create and set the price of new ERC20 tokens.

Deploy in any EVM compatible chain, in a single transaction.

## Features

- Create an ERC20 token with fixed total supply
- Supply liquidity in a decentralized exchange of choice
- Verify contract source code in official explorers like etherscan
